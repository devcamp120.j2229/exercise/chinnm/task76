package com.task7405.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task7405.restapi.model.Customer;
import com.task7405.restapi.repository.CustomerRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customer_detail_by_fristname/{firstname}")
    public ResponseEntity<Object> getUserById(@PathVariable(name = "firstname", required = true) String firstname) {
        List<Customer> userFounded = customerRepository.findCustomersByFirstNameLike(firstname);

        if (!userFounded.isEmpty()) {
            return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/customer_by_country_orderby_firstname/{country}/{numberPage}")
    // @GetMapping("/customer_by_country_orderby_firstname/{country}")
    public ResponseEntity<List<Customer>> getCustomerByCountryOrderByFirstname(
            @PathVariable("country") String country, @PathVariable("numberPage") int numberPage) {
        // @PathVariable("country") String country) {

        int length = 3;
        int start = numberPage - 1;
        try {

            List<Customer> listCustomer = customerRepository.findCustomersByCountryOrderByFirstName(country,
                    PageRequest.of(start, length));
            if (!listCustomer.isEmpty()) {
                return new ResponseEntity<>(listCustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer_by_city_and_state/{cỉty}/{state}/{numberPage}")
    public ResponseEntity<List<Customer>> getCustomerByCityAndState(
            @PathVariable("city") String city, @PathVariable("state") String state,
            @PathVariable("numberPage") int numberPage) {
        // @PathVariable("country") String country) {

        int length = 3;
        int start = numberPage - 1;
        try {

            List<Customer> listCustomer = customerRepository.findCustomersByCityAndState(city, state,
                    PageRequest.of(start, length));
            if (!listCustomer.isEmpty()) {
                return new ResponseEntity<>(listCustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update_contry/{country}/{id}")

    public ResponseEntity<Object> updateCountry(@PathVariable("country") String country,
            @PathVariable("id") int id) {
        try {
            System.out.println("--------------------------------------");
            System.out.println("----------------LAM DAN DAY-------------------------");
            int countryUpdate = customerRepository.updateCountry(country, id);

            return new ResponseEntity<>(countryUpdate, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getMessage());
        }
    }

}
