package com.task7405.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.task7405.restapi.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query(value = "SELECT * FROM products WHERE id =:id", nativeQuery = true)
    Optional<Product> findProductById(@Param("id") int id);

    @Query(value = " SELECT products.* FROM products LEFT JOIN product_lines ON products.product_line_id = product_lines.id WHERE product_lines.id =:productLineId", nativeQuery = true)
    List<Product> findProductByProductLineId(@Param("productLineId") int productLineId);

}
