package com.task7405.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task7405.restapi.model.ProductLine;
import com.task7405.restapi.repository.ProductLineRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class ProductLineController {
    @Autowired
    ProductLineRepository productLineRepository;

    @PostMapping("/product_line/create")
    public ResponseEntity<Object> createProductLine(@RequestBody ProductLine productLine) {
        try {
            ProductLine newRole = new ProductLine();
            newRole.setProductLine(productLine.getProductLine());
            newRole.setTextDescription(productLine.getTextDescription());

            ProductLine savedRole = productLineRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/product_line/update/{id}")
    public ResponseEntity<Object> updateProductLine(@PathVariable("id") int id, @RequestBody ProductLine productLine) {
        try {
            Optional<ProductLine> productlineData = productLineRepository.findProductLineById(id);
            if (productlineData.isPresent()) {
                ProductLine newProductLine = productlineData.get();
                newProductLine.setProductLine(productLine.getProductLine());
                newProductLine.setTextDescription(productLine.getTextDescription());

                ProductLine savedProductLine = productLineRepository.save(newProductLine);
                return new ResponseEntity<>(savedProductLine, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified productline: " + id + "  for update.");
        }

    }

    @CrossOrigin
    @GetMapping("/product_line/details/{id}")
    public ResponseEntity<ProductLine> getProductLineById(@PathVariable int id) {
        try {
            Optional<ProductLine> productLineData = productLineRepository.findProductLineById(id);
            if (productLineData.isPresent()) {
                return new ResponseEntity<>(productLineData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/product_line/all")
    public ResponseEntity<List<ProductLine>> getAllProductLine() {
        try {
            List<ProductLine> listproductline = new ArrayList<ProductLine>();
            productLineRepository.findAll().forEach(listproductline::add);

            return new ResponseEntity<>(listproductline, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/productline/delete/{id}")
    public ResponseEntity<Object> deleteProductLineById(@PathVariable int id) {
        try {
            Optional<ProductLine> productLineData = productLineRepository.findProductLineById(id);
            ProductLine productLineDelete = productLineData.get();
            productLineRepository.delete(productLineDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
